$(function() {
    // punchlines!
    var p = [
        { m: 'before being unceremoniously relegated into obscurity', c: true },
        { m: "none of whom give a shit" },
    ];

    var pIdx = Math.round(Math.random() * (p.length - 1));
    $('#punch').html(p[pIdx].m);
    if (p[pIdx].c !== true) { $('.committee').remove(); }

    // shrug animation :)
    var fIdx = 0;
    var f = [ '¯&bsol;_(ツ)_/¯',
              '¯-_(ツ)_-¯',
              '--_(ツ)_--',
              '-__(ツ)__-',
              '___(ツ)___',
              '-__(ツ)__-',
              '--_(ツ)_--',
              '¯-_(ツ)_-¯',
              '¯&bsol;_(ツ)_/¯',
    ];

    var face = $('#face');
    var updateFace = function() {
        if (fIdx < f.length) {
            face.html('<span>' + f[fIdx] + '</span>');
        }
        fIdx++;
        if (fIdx > f.length * 2) {
            fIdx = 0;
        }
    };
    updateFace();
    setInterval(updateFace, 200);

    $('.appear').each(function() {
        var el = $(this);
        setTimeout(function() {
            el.hide();
            setTimeout(function() { el.css('visibility', 'visible'); }, 100);
            el.fadeIn(2000);
        }, 2000 * el.data('o'));
    });

});
